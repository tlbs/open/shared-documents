# Shared Documents

General shared files for reference for all people.
Relevant for Gebauer, Neo & Co

## Relevant documents
### [.gitignore](/.gitignore)
The general template that can be used for each repository. Relevant for C# projects, especially TimeLine custom paths.  
Take this file template and add it to your own repository.
>  **Important:** To create a new .gitignore file in windows, simple do the following:  
> 1. Shift-Right-Click in your project folder, choose "Open Command Prompt"  
> 1. Type `copy NUL .gitignore`

### [.gitaliases](/.gitaliases)
A helpful file of git aliases that can be used via git commands (Git Bash, Powershell, CMD).  
Initial file/commands designed by @chusung-tl.

To configure those aliases locally, follow these steps:
> 1. Checkout this repository or copy the .gitaliases file somewhere locally onto your PC
> 1. Open the file `%USERPROFILE%\.gitconfig` (your windows user folder) in a text editor (Notepad++, VS Code, etc)
> 1. Add the following at the bottom, replacing the path with the path to your .gitaliases  
> 
>  **Important:** Make sure to escacape `\` in your path with `\\`, otherwise git breaks
> ```
> [include]
>     path = C:\\path\\to\\your\\.gitaliases
> ```
