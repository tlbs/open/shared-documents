[alias]
    ##############################
    # Config - REPLACE IF NEEDED #
    ##############################
    # Get the main branch name (specified in this alias here. Can be overriden in project-specific aliases if needed)
    # Override this with the name branch name of your personal preference that you use. (e. g. "main", "master" or "develop")
    # This is the branch from which you want to create feature branches.
    config-main-branch = !"echo main"


    #########################
    # Needed base functions #
    #########################
    # Get the current branch name (not so useful in itself, but used in other aliases)
    branch-name = !"git rev-parse --abbrev-ref HEAD"
    # Creates given branch based on main and deletes the one original one with the same name
    create-with-delete = !"f() { git ensure-main git checkout $(git config-main-branch) && git branch -d $@ && git checkout -b $@; }; f"


    ####################
    # Simple Shortcuts #
    ####################
    # Go to the branch with given name
    go = "checkout"
    # Gets the given remote branch locally and set to track it
    get = "checkout -t"
    # Go back to main branch
    return = !"git ensure-main git checkout $(git config-main-branch)"
    # Removes the last commit
    undo-commit = reset --soft HEAD~1

    ####################
    # Primary Commands #
    ####################
    # Creates given branch based on master and checks it out. Just a shortcut of git checkout -b
    work = !"f() { git return && git checkout -b $@; }; f"
    # Refresh the current branch from the upstream/origin
    refresh = !"f() { git pull $(git ref-repository) $(git branch-name) && git push; }; f"
    # Push the current branch to the remote "origin", and set it to track the upstream branch
    publish = !"git push -u origin $(git branch-name)"
    # Delete the remote version of the current branch
    unpublish = "!git push origin :$(git branch-name)"
    # Resets the current branch to origin
    reset-to-origin = !"git reset --hard origin/$(git branch-name)"
    # Recreates current branch based on main
    recreate = !"f() { git create-with-delete $(git branch-name); }; f"
    # Remove all branches that got already merged
    tidy = !"git return && git refresh && git fetch --all -p && git branch -vv | grep ': gone]' | awk '{ print $1 }' | xargs -n 1 git branch -D"

    ###########
    # Helpers #
    ###########
    # Verifies that the main branch exists as defined in the alias
    verify-main = !"git ensure-main echo 'Main branch `'$(git config-main-branch)'` exists. Verified.'"
    # Lists all existing aliases
    list-aliases = "!git config -l | grep alias | cut -c 7-"
    # Create a Test branch based on main
    test = !"git work test"
    # Deletes the test branch and goes back to main
    test-finished = !"git return && git branch -D test"

    ######################
    # Internal functions #
    ######################
    # A wrapper around a command, only executing it if the configured main branch does exist. Otherwise prints a warning
    ensure-main = !"f() { git show-ref --verify --quiet refs/heads/$(git config-main-branch); if [[ $? == 0 ]]; then ( $@ ); else echo 'WARNING! Main branch `'$(git config-main-branch)'` does not exist!'; fi; }; f"
    # Return "upstream" if an upstream is defined, otherwise origin
    ref-repository = !"f() { git remote | grep upstream > /dev/null; if [[ $? == 0 ]]; then echo 'upstream'; else echo 'origin'; fi; }; f"